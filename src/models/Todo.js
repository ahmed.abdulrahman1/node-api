const mongoose = require('mongoose');



 let TodoSchema = new mongoose.Schema({
   
     title: {
         type: String,
         required: true
     },
     isComplete: Boolean
 })

 module.exports = mongoose.model("Todo", TodoSchema)
