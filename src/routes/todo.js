let Todo = require("../models/Todo")
let express = require("express")
let router = express.Router()



//get one object
router.get("/:id", async (req, res) => {
    try {
        const todo = await Todo.findById(req.params.id)
        res.json(todo)
    }
    catch (error) {
        res.json({message: error})
    }
})

//get all to-do objects
router.get("/", async (req, res) => {
    try {
        const todos = await Todo.find()
        res.json(todos)
    }
    catch (error) {
        res.json( {message: err} )
    }
})

//post a to-do item

router.post("/", async (req, res) => {
  const { title }  = req.body;
  
  const todo = new Todo({
    title,
    isComplete : false
  });
  try {
    const newTodo = await todo.save();
    res.json(newTodo)
    
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
 
  console.log(req.body)
});

// update a to-do item

router.put("/:id", async (req, res) => {
    try {
  
        const updatedTodo = await Todo.updateOne(
            { title: req.body.title}
        )
        res.json(updatedTodo)

    }
    catch (error) {
        res.json({ message: error })
    }
})


// delete a to-do item

router.delete("/:id", async (req, res) => {
    try {
        const id = req.params.id
        const removedTodo = await Todo.remove({ _id: id })
        res.json(removedTodo);
    }
    catch (error) {
        res.json({ message: error })
    }
})




module.exports = router