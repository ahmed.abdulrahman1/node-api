let express = require("express")
let app = express()
let todoRoute = require("./routes/todo")
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
const PORT = 8080

const db = "mongodb+srv://HunchoJack:123@cluster0.mweqs.mongodb.net/rest-api?retryWrites=true&w=majority"


//body parser
app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(bodyParser.json());
// make the app use todo's route
app.use(todoRoute)



//Mongo connection
const connectDB = async () => {
    try {
      await mongoose.connect(db, { useNewUrlParser: true });
      console.log("mongo db connected...");
    } catch (err) {
      console.error(err.message);
      process.exit(1);
    }
  };
  connectDB();




// prints current time
app.use( (req, res, next) => {
    console.log(`${new Date().toString()} => ${req.originalUrl}`)
    next()
} )



// make the app use the public file as our main file
app.use(express.static("public"))

app.use( (req, res, next) => {
    res.status(404).send("Are you lost like Joe Biden?")
} )



app.listen(PORT, () => console.info(
    `Server is running on ${PORT}`
) )


